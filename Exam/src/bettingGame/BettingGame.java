package bettingGame;

import java.util.Random;
import java.util.regex.Pattern;

public class BettingGame {
	//determines if the user won give the results and their guess
	private static boolean didWin(String guess, int result) {
		if (result%2 ==0 && guess.equals("even")) {
			return true;
		}
		else if (result%2 != 0 && guess.equals("odd")) {
			return true;
		}
		return false;
	}
	
	//returns a random number to simulate rolling a die
	private static int rollDie() {
		Random rand = new Random();
		return rand.nextInt(6)+1;
	}
	
	//takes a player as input and rolls a die to see if they won
	//if they did it adjusts their money and prints to the user
	private static void determineResults(Player player) {
		int result = rollDie();
		if (didWin(player.getGuess(), result)) {
			player.adjustMoney(true);
			System.out.println("congratulations you won! you now have: "+ player.getMoney()+"$");
		}
		else{
			player.adjustMoney(false);
			System.out.println("unfortunately you lost. you now have:  "+ player.getMoney()+"$");
		}
	}
	
	//makes sure the bet matches a regex representing only digits and 
	//that the bet is less than their total money
	private static boolean validateBet(Player user) {
		if(!Pattern.matches("[0-9]+$", String.valueOf(user.getBet()))) {
			return false;
		}
		if(user.getBet() > user.getMoney()) {
			return false;
		}
		return true;
	}
	
	//calls the helper methods to form the game
	public static Player play(Player user) {
		if (!validateBet(user)){
			System.out.println("please enter a valid bet");
			return user;
		}
		determineResults(user);
		return user;
		
	}
}
