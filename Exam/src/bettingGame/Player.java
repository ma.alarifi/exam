package bettingGame;

//represents the user stores their money, bet, and guess
public class Player {
	int money;
	int bet;
	String guess;

	public Player() {
		money = 250;
		bet = 0;
		guess = null;
	}
	
	//adjusts their money based on whether they have won or not
	public void adjustMoney(boolean hasWon) {
		if (hasWon) {
			money = money+bet;
		}
		else {
			money = money-bet;
		}
	}
	
	public int getMoney() {
		return money;
	}
	
	public int getBet() {
		return bet;
	}
	
	public void setBet(int bet) {
		this.bet = bet;
	}
	
	public void setGuess(String guess) {
		this.guess = guess;
	}

	public String getGuess() {
		return guess;
	}
}
