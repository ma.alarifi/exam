package bettingGame;

//I had technical difficulties during this question
//essentially my power supply on my desktop failed so I had to switch
//to my chromebook which doesn't have all of the development software so I had
//to take time to install and I wasn't able to get it to display properly with javafx at the end
//so I didn't get to test my work for this question.
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;

public class BettingGameApplication extends Application{
	private Player user;
	
	//creates the ui and places the event handlers.
	public void start(Stage stage) {
		
		Scene scene = new Scene(new Group(), 500, 250);
		stage.setTitle("Betting Game");
		TextField bet = new TextField();
		Button odd = new Button("Odd");
		Button even = new Button("Even");
		GridPane grid = new GridPane();
		
		grid.add(new Label("Bet"), 0, 0);
		grid.add(bet, 0, 0);
		grid.add(odd, 0, 0);
		grid.add(even, 0, 0);
		
		//sets handlers on the buttons to play the game on click with the guess
		//corresponding to the button clicked
		odd.setOnAction(new EventHandler<ActionEvent>() {
 
            public void handle(ActionEvent event) {
                user.setGuess("odd");
                user.setBet(Integer.parseInt(bet.getText()));
                user = BettingGame.play(user);
            }
        });
		
		//sets handlers on the buttons to play the game on click with the guess
		//corresponding to the button clicked
		even.setOnAction(new EventHandler<ActionEvent>() {
			 
            public void handle(ActionEvent event) {
                user.setGuess("even");
                user.setBet(Integer.parseInt(bet.getText()));
                user = BettingGame.play(user);
            }
        });
        
		Group root = (Group) scene.getRoot();
		root.getChildren().add(grid);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 *  Main method that launches the application
	 */
	public static void main(String[]args) {
		Application.launch(args);
	}

}
