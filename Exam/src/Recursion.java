
public class Recursion {

	public static void main(String[] args) {
		String[] words = {"q","w","w","m","q","q","q"};
		System.out.println(recursiveCount(words, 3));

	}
	
	public static int recursiveCount(String[] words, int n) {
		if (n%2 != 0 ) {
			n++;
		}
		int result = 0;
		
		if (words[n].contains("q")) {
			result++;
		}
		if(n+2 < words.length) {
			return result + recursiveCount(words, n+2);
		}
		return result;
	}

}
